package com.kz.rep;

import org.hibernate.SessionFactory;

import com.kz.models.Cliente;
import com.kz.utils.AbsFacade;
import com.kz.utils.Dao;
import com.kz.utils.Hib;

public class ClienteImpl extends AbsFacade<Cliente> implements Dao<Cliente>{

	private SessionFactory sessionFactory;
	
	public ClienteImpl() {
		super(Cliente.class);
		sessionFactory = Hib.getSessionFactory();
	}
	
	@Override
	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}


}