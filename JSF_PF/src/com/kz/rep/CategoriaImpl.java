package com.kz.rep;

import org.hibernate.SessionFactory;

import com.kz.models.Categoria;
import com.kz.utils.AbsFacade;
import com.kz.utils.Dao;
import com.kz.utils.Hib;

public class CategoriaImpl extends AbsFacade<Categoria> implements Dao<Categoria>{

	private SessionFactory sessionFactory;
	
	public CategoriaImpl() {
		super(Categoria.class);
		this.sessionFactory = Hib.getSessionFactory();
	}
	
	@Override
	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}
}
