package com.kz.rep;

import org.hibernate.SessionFactory;

import com.kz.models.Factura;
import com.kz.utils.AbsFacade;
import com.kz.utils.Dao;
import com.kz.utils.Hib;

public class FacturaImpl extends AbsFacade<Factura> implements Dao<Factura>{

	private SessionFactory sessionFactory;
	
	public FacturaImpl() {
		super(Factura.class);
		sessionFactory = Hib.getSessionFactory();
	}
	
	@Override
	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}


}