package com.kz.rep;

import org.hibernate.SessionFactory;

import com.kz.models.Detalle;
import com.kz.utils.AbsFacade;
import com.kz.utils.Dao;
import com.kz.utils.Hib;

public class DetalleImpl extends AbsFacade<Detalle> implements Dao<Detalle>{

	private SessionFactory sessionFactory;
	
	public DetalleImpl() {
		super(Detalle.class);
		sessionFactory = Hib.getSessionFactory();
	}
	
	@Override
	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}


}
