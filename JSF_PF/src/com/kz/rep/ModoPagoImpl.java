package com.kz.rep;

import org.hibernate.SessionFactory;

import com.kz.models.ModoPago;
import com.kz.utils.AbsFacade;
import com.kz.utils.Dao;
import com.kz.utils.Hib;

public class ModoPagoImpl extends AbsFacade<ModoPago> implements Dao<ModoPago>{

	private SessionFactory sessionFactory;
	
	public ModoPagoImpl() {
		super(ModoPago.class);
		sessionFactory = Hib.getSessionFactory();
	}
	
	@Override
	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}


}
