package com.kz.rep;

import org.hibernate.SessionFactory;

import com.kz.models.Producto;
import com.kz.utils.AbsFacade;
import com.kz.utils.Dao;
import com.kz.utils.Hib;

public class ProductoImpl extends AbsFacade<Producto> implements Dao<Producto>{

	private SessionFactory sessionFactory;
	
	public ProductoImpl() {
		super(Producto.class);
		sessionFactory = Hib.getSessionFactory();
	}
	
	@Override
	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}


}
