package com.kz.controls;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.kz.models.Categoria;
import com.kz.models.Producto;
import com.kz.rep.CategoriaImpl;
import com.kz.rep.ProductoImpl;
import com.kz.utils.Enlace;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;

@ManagedBean
public class Bodega {

	private Categoria categoria;
	private Producto producto;
	private CategoriaImpl catImp = new CategoriaImpl();
	private ProductoImpl proImp = new ProductoImpl();

	public Bodega() {
		categoria = new Categoria();
		producto = new Producto();
	}

	/* Metodos especificos */

	public List<Producto> consultarBodega() {
		return proImp.findAll();
	}

	public List<Categoria> consultarRubros() {
		return catImp.findAll();
	}

	public void repCategorias() {
		try {
			File jasper = new File(
					FacesContext.getCurrentInstance().getExternalContext().getRealPath("resources/exports/otrocliente.jasper"));
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("param", "KZ");
			JasperPrint jasperprint = JasperFillManager.fillReport(jasper.getPath(), parametros, Enlace.conecta());

			/* Visualizar en el navegador */
			byte[] bytes = JasperRunManager.runReportToPdf(jasper.getPath(), parametros, Enlace.conecta());
			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			response.setContentType("application/pdf");
			response.setContentLength(bytes.length);
			ServletOutputStream os = response.getOutputStream();
			os.write(bytes, 0, bytes.length);
			os.flush();
			os.close();
			FacesContext.getCurrentInstance().responseComplete();

			/* Descargar pdf con JSF */
//	        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
//	        response.addHeader("Content-disposition", "attachment; filename=reporte.pdf");
//	        ServletOutputStream printer = response.getOutputStream();
//	        JasperExportManager.exportReportToPdfStream(jasperprint, printer);
//	        FacesContext.getCurrentInstance().responseComplete();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/* Getter y Setter */

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public CategoriaImpl getCatImp() {
		return catImp;
	}

	public void setCatImp(CategoriaImpl catImp) {
		this.catImp = catImp;
	}

	public ProductoImpl getProImp() {
		return proImp;
	}

	public void setProImp(ProductoImpl proImp) {
		this.proImp = proImp;
	}

}
