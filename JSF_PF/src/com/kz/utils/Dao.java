package com.kz.utils;

import java.util.List;

public interface Dao<T> {
	public void create(T e);

	public void delete(T e);

	public void update(T e);

	public T find(Object id);

	public List<T> findAll();
}
