
package com.kz.utils;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 * @author KZ
 */
public abstract class AbsFacade<T> {

	private Class<T> entityClass;

	public AbsFacade(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	public abstract SessionFactory getSessionFactory();

	public void create(T entity) {
		Transaction t = getSessionFactory().getCurrentSession().getTransaction();
		t.begin();
		getSessionFactory().getCurrentSession().save(entity);
		t.commit();
	}

	public void update(T entity) {
		Transaction t = getSessionFactory().getCurrentSession().getTransaction();
		t.begin();
		getSessionFactory().getCurrentSession().update(entity);
		t.commit();
	}

	public void delete(T entity) {
		Transaction t = getSessionFactory().getCurrentSession().getTransaction();
		t.begin();
		getSessionFactory().getCurrentSession().delete(entity);
		t.commit();
	}

	public T find(Object id) {
		Transaction t = getSessionFactory().getCurrentSession().getTransaction();
		t.begin();
		T vol = getSessionFactory().getCurrentSession().find(entityClass, id);
		t.commit();
		return vol;
	}

	public List<T> findAll() {
		Transaction t = getSessionFactory().getCurrentSession().getTransaction();
		t.begin();
		CriteriaQuery cq = getSessionFactory().getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		List<T> ls = getSessionFactory().getCurrentSession().createQuery(cq).getResultList();
		t.commit();
		getSessionFactory().getCurrentSession().close();
		return ls;

//      2da forma
//		return getSessionFactory().getCurrentSession().createQuery("from " + entityClass.getName()).getResultList();

//      3ra forma
//      CriteriaQuery<T> cq = (CriteriaQuery<T>) getSessionFactory().getCriteriaBuilder().createQuery();
//      cq.select(cq.from(entityClass));
//    	Query query = getSessionFactory().getCurrentSession().createQuery(cr);
//    	List<T> results = query.getResultList();
//    	t.commit();

//      4ta forma
//		Transaction t = getSessionFactory().getCurrentSession().beginTransaction();
//    	CriteriaBuilder cb = getSessionFactory().getCurrentSession().getCriteriaBuilder();
//    	CriteriaQuery<T> cr = cb.createQuery(entityClass);
//    	Root<T> root = cr.from(entityClass);
//    	cr.select(root);
//		Query query = getSessionFactory().getCurrentSession().createQuery(cr);
//    	List<T> results = query.getResultList();
//    	t.commit();
		
	}

}
