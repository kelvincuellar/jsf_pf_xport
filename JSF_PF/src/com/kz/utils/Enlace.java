package com.kz.utils;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Enlace {
    private static Connection c;

    public static Connection conecta(){
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            c = DriverManager.getConnection("jdbc:mysql://localhost:3306/facturacion?useSSL=false", "root", "admin");
//            PreparedStatement p = c.prepareStatement("select * from cliente order by id_cliente");
//            ResultSet r = p.executeQuery();
            System.out.println("Conexion exitosa=");
        } catch (SQLException e) {
            System.out.println("Algo ocurre");
            c = null;
        }
        catch(ClassNotFoundException x){
            System.out.println("Que no deja que podamos conectar");
            c = null;
        }
        return c;
    }

    public static void cierra() {
        try {
            if (c != null) {
                if (!c.isClosed()) {
                    c.close();
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error al cerrar la conexion: " + ex.toString());
        }
    }
}
